//
//  MenuViewController.swift
//  AKSwiftSlideMenu
//
//  Created by Ashish on 21/09/15.
//  Copyright (c) 2015 Kode. All rights reserved.
//

import UIKit
let WEBSITE_LINK = "http://www.gujaratilexicon.com"

protocol SlideMenuDelegate
{
    func slideMenuItemSelectedAtIndex(_ index : Int32)
}

class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    /**
    *  Array to display menu options
    */
    @IBOutlet var tblMenuOptions : UITableView!
    
    /**
    *  Transparent button to hide menu
    */
    @IBOutlet var btnCloseMenuOverlay : UIButton!
    
    /**
    *  Array containing menu options
    */
    var arrayMenuOptions = [Dictionary<String,String>]()
    
    /**
    *  Menu button which was tapped to display the menu
    */
    var btnMenu : UIButton!
   
    /**
    *  Delegate of the MenuVC
    */
    var delegate : SlideMenuDelegate?
    @IBOutlet var viewHeigh : NSLayoutConstraint!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tblMenuOptions.tableFooterView = UIView()
        self.tblMenuOptions.tableFooterView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.tblMenuOptions.frame.size.width, height: 1))
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
        override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        
        UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.lightContent, animated: true)

        updateArrayMenuOptions()
    }
    
    func updateArrayMenuOptions()
    {
        arrayMenuOptions.append(["title":"About Us", "icon":"aboutus"])
        arrayMenuOptions.append(["title":"Contact Us", "icon":"contactus"])
        arrayMenuOptions.append(["title":"Share", "icon":"share"])
        arrayMenuOptions.append(["title":"Rate Us", "icon":"rateus"])
        arrayMenuOptions.append(["title":"More Apps", "icon":"moreapps"])
        tblMenuOptions.reloadData()
    }   
    
    @IBAction func onProfileClick(_ button:UIButton!)
    {
//        base.slideMenuItemSelectedAtIndex(-1);
        
        //        sender.tag = 0;
        onCloseMenuClick(button)
    
    }
    @IBAction func onWebsiteClick(_ button:UIButton!)
    {
        UIApplication.shared.openURL(URL(string : WEBSITE_LINK)!)
    }
    
    
    
    @IBAction func onCloseMenuClick(_ button:UIButton!)
    {
        btnMenu.tag = 0
        
        if (self.delegate != nil)
        {
            var index = Int32(button.tag)
            if(button == self.btnCloseMenuOverlay)
            {
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
       
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            
            self.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
            self.btnCloseMenuOverlay.backgroundColor = UIColor.clear
            self.btnCloseMenuOverlay.alpha = 1
            }, completion: { (finished) -> Void in
                self.navigationItem.rightBarButtonItem?.isEnabled = true;
                self.view.removeFromSuperview()
                self.removeFromParentViewController()
        })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellMenu")!
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.layoutMargins = UIEdgeInsets.zero
        cell.preservesSuperviewLayoutMargins = false
        cell.backgroundColor = UIColor.clear
        
        let lblTitle : UILabel = cell.contentView.viewWithTag(101) as! UILabel
        let imgIcon : UIImageView = cell.contentView.viewWithTag(100) as! UIImageView
        
        imgIcon.image = UIImage(named: arrayMenuOptions[indexPath.row]["icon"]!)
        lblTitle.text = arrayMenuOptions[indexPath.row]["title"]!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let btn = UIButton(type: UIButtonType.custom)
        btn.tag = indexPath.row
        self.onCloseMenuClick(btn)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayMenuOptions.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1;
    }
}
