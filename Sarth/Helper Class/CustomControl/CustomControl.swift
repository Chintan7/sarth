//
//  CustomControl.swift
//
//  Created by Ankit on 3/30/16.
//  Copyright © 2016 Ankit. All rights reserved.
//

import Foundation
import UIKit


extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
}


//extension UIButton {
//    @IBInspectable var bottomBorder: CGFloat {
//        get {
//            return self.bottomBorder
//        }
//        set {
//            let lineView = UIView(frame: CGRectMake(0, 0, self.frame.size.width, bottomBorder))
//            lineView.backgroundColor=UIColor(red: 63.0/255.0, green: 63.0/255.0, blue: 63.0/255.0, alpha: 1)
//            self.addSubview(lineView)
//        }
//    }
//}
