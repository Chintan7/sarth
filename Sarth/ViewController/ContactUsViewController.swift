//
//  ContactUsViewController.swift
//  Sarth
//
//  Created by Hetal Govani on 08/09/17.
//  Copyright © 2017 SoluLab. All rights reserved.
//

import UIKit
import GoogleMobileAds
import CoreLocation
import MapKit
import MessageUI
class ContactUsViewController: BaseViewController,GADBannerViewDelegate,MFMailComposeViewControllerDelegate {
    //@IBOutlet weak var map: MKMapView!
    @IBOutlet var headerHeight : NSLayoutConstraint!

    @IBOutlet weak var img_ContactUsMap: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if Display.typeIsLike == DisplayType.iphoneX
        {
            headerHeight.constant = 84
        }
        else
        {
            headerHeight.constant = 64
        }
        let bannerView = GADBannerView.init(frame: CGRect.init(x: 0, y: self.view.frame.height - 50, width: self.view.frame.width, height: 50))
        self.view.addSubview(bannerView)
        bannerView.adUnitID = ADS_UNIT_ID
        bannerView.rootViewController = self
        bannerView.delegate = self
        
        bannerView.load(GADRequest())
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString("303-A, Aditya Arcade,Nr. Choice Restaurant,Swastik Cross Road,Navrangpura,Ahmedabad 380009,Gujarat, India")
        {
            placemarks, error in
            let placemark = placemarks?.first
            let lat = placemark?.location?.coordinate.latitude
            let lon = placemark?.location?.coordinate.longitude
            print("Lat: \(String(describing: lat)), Lon: \(String(describing: lon))")
            
            let newPin = MKPointAnnotation()
            newPin.coordinate = (placemark?.location?.coordinate)!
            //self.map.addAnnotation(newPin)
            
            let center = CLLocationCoordinate2D(latitude: lat!, longitude: lon!)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            
            //set region on the map
           // self.map.setRegion(region, animated: true)
        }
        //
        // Do any additional setup after loading the view.
    }
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnMailPress(sender : UIButton)
    {
        if( MFMailComposeViewController.canSendMail() )
        {
            let emailTitle = "Gujarati Lexicon"
            let toRecipents = ["info@gujaratilexicon.com"]
            let mc: MFMailComposeViewController = MFMailComposeViewController()
            mc.mailComposeDelegate = self
            mc.setSubject(emailTitle)
            mc.setToRecipients(toRecipents)
            mc.navigationBar.barTintColor = UIColor.white
            self.present(mc, animated: true, completion: nil)
        }
    }
    @IBAction func onWebsiteClick(_ button:UIButton!)
    {
        UIApplication.shared.openURL(URL(string : "http://www.gujaratilexicon.com")!)
    }
    @IBAction func btncallPress(sender : UIButton)
    {
        let alert = UIAlertController.init(title: "Gujarati Lexicon", message: "Would you like to call  +91-79-48909758", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Yes", style: UIAlertActionStyle.default, handler: { (alert:UIAlertAction) in
            guard let number = URL(string: "tel://" + " +91-79-48909758") else { return }
            UIApplication.shared.open(number)
        }))
        alert.addAction(UIAlertAction.init(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?)
    {
        switch result
        {
        case .cancelled:
            print("Mail cancelled")
        case .saved:
            print("Mail saved")
        case .sent:
            print("Mail sent")
        case .failed:
            print("Mail sent failure: \(String(describing: error?.localizedDescription))")
        default:
            break
        }
        self.dismiss(animated: true, completion: nil)
    }

    // MARK: - Bannerview delegate
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView)
    {
        print("adViewDidReceiveAd")
        //        gadBannerObj = bannerView
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
