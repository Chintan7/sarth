//
//  AboutViewController.swift
//  Sarth
//
//  Created by Hetal Govani on 08/09/17.
//  Copyright © 2017 SoluLab. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AboutViewController: BaseViewController,GADBannerViewDelegate {
    @IBOutlet var headerHeight : NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        if Display.typeIsLike == DisplayType.iphoneX
        {
            headerHeight.constant = 84
        }
        else
        {
            headerHeight.constant = 64
        }
        let bannerView = GADBannerView.init(frame: CGRect.init(x: 0, y: self.view.frame.height - 50, width: self.view.frame.width, height: 50))
        self.view.addSubview(bannerView)
        bannerView.adUnitID = ADS_UNIT_ID
        bannerView.rootViewController = self
        bannerView.delegate = self
        
        bannerView.load(GADRequest())
        // Do any additional setup after loading the view.
    }
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Bannerview delegate
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView)
    {
        print("adViewDidReceiveAd")
        //        gadBannerObj = bannerView
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
