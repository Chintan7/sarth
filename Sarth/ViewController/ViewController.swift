//
//  ViewController.swift
//  Sarth
//
//  Created by Hetal Govani on 07/09/17.
//  Copyright © 2017 SoluLab. All rights reserved.
//

import UIKit
import GoogleMobileAds
import ReachabilitySwift
import MBProgressHUD

let SELECT_DB_QUERY = "select * from sarth"

let TABLE_VIEW_CELL = "UITableViewCell"
let KKHGKEY = "ક,ખ,ગ,ઘ,ઙ,ચ,છ,જ,ઝ,ઞ,ટ,ઠ,ડ,ઢ,ણ,ત,થ,દ,ધ,ન,પ,ફ,બ,ભ,મ,ય,ર,લ,ળ,વ,શ,ષ,સ,હ"

let KEYk = "ક્ક,ક્ખ,ક્ગ,ક્ઘ,ક્ઙ,ક્ચ,ક્છ,ક્જ,ક્ઝ,ક્ઞ,ક્ટ,ક્ઠ,ક્ડ,ક્ઢ,ક્ણ,ક્ત,ક્થ,ક્દ,ક્ધ,ક્ન,ક્પ,ક્ફ,ક્બ,ક્ભ,ક્મ,ક્ય,ક્ર,ક્લ,ક્ળ,ક્વ,ક્શ,ક્ષ,ક્સ,ક્હ"

let KEYg = "ગ્ક,ગ્ખ,ગ્ગ,ગ્ઘ,ગ્ઙ,ગ્ચ,ગ્છ,ગ્જ,ગ્ઝ,ગ્ઞ,ગ્ટ,ગ્ઠ,ગ્ડ,ગ્ઢ,ગ્ણ,ગ્ત,ગ્થ,ગ્દ,ગ્ધ,ગ્ન,ગ્પ,ગ્ફ,ગ્બ,ગ્ભ,ગ્મ,ગ્ય,ગ્ર,ગ્લ,ગ્ળ,ગ્વ,ગ્શ,ગ્ષ,ગ્સ,ગ્હ"

let KEYdah = "ઙ્ક,ઙ્ખ,ઙ્ગ,ઙ્ઘ,ઙ્ઙ,ઙ્ચ,ઙ્છ,ઙ્જ,ઙ્ઝ,ઙ્ઞ,ઙ્ટ,ઙ્ઠ,ઙ્ડ,ઙ્ઢ,ઙ્ણ,ઙ્ત,ઙ્થ,ઙ્દ,ઙ્ધ,ઙ્ન,ઙ્પ,ઙ્ફ,ઙ્બ,ઙ્ભ,ઙ્મ,ઙ્ય,ઙ્ર,ઙ્લ,ઙ્ળ,ઙ્વ,ઙ્શ,ઙ્ષ,ઙ્સ,ઙ્હ"

let KEYchh = "છ્ક,છ્ખ,છ્ગ,છ્ઘ,છ્ઙ,છ્ચ,છ્છ,છ્જ,છ્ઝ,છ્ઞ,છ્ટ,છ્ઠ,છ્ડ,છ્ઢ,છ્ણ,છ્ત,છ્થ,છ્દ,છ્ધ,છ્ન,છ્પ,છ્ફ,છ્બ,છ્ભ,છ્મ,છ્ય,છ્ર,છ્લ,છ્ળ,છ્વ,છ્શ,છ્ષ,છ્સ,છ્હ"

let KEYz = "ઝ્ક,ઝ્ખ,ઝ્ગ,ઝ્ઘ,ઝ્ઙ,ઝ્ચ,ઝ્છ,ઝ્જ,ઝ્ઝ,ઝ્ઞ,ઝ્ટ,ઝ્ઠ,ઝ્ડ,ઝ્ઢ,ઝ્ણ,ઝ્ત,ઝ્થ,ઝ્દ,ઝ્ધ,ઝ્ન,ઝ્પ,ઝ્ફ,ઝ્બ,ઝ્ભ,ઝ્મ,ઝ્ય,ઝ્ર,ઝ્લ,ઝ્ળ,ઝ્વ,ઝ્શ,ઝ્ષ,ઝ્સ,ઝ્હ"

let KEYta = "ટ્ક,ટ્ખ,ટ્ગ,ટ્ઘ,ટ્ઙ,ટ્ચ,ટ્છ,ટ્જ,ટ્ઝ,ટ્ઞ,ટ્ટ,ટ્ઠ,ટ્ડ,ટ્ઢ,ટ્ણ,ટ્ત,ટ્થ,ટ્દ,ટ્ધ,ટ્ન,ટ્પ,ટ્ફ,ટ્બ,ટ્ભ,ટ્મ,ટ્ય,ટ્ર,ટ્લ,ટ્ળ,ટ્વ,ટ્શ,ટ્ષ,ટ્સ,ટ્હ"


let KEYd = "ડ્ક,ડ્ખ,ડ્ગ,ડ્ઘ,ડ્ઙ,ડ્ચ,ડ્છ,ડ્જ,ડ્ઝ,ડ્ઞ,ડ્ટ,ડ્ઠ,ડ્ડ,ડ્ઢ,ડ્ણ,ડ્ત,ડ્થ,ડ્દ,ડ્ધ,ડ્ન,ડ્પ,ડ્ફ,ડ્બ,ડ્ભ,ડ્મ,ડ્ય,ડ્ર,ડ્લ,ડ્ળ,ડ્વ,ડ્શ,ડ્ષ,ડ્સ,ડ્હ"

let KEYna = "ણ્ક,ણ્ખ,ણ્ગ,ણ્ઘ,ણ્ઙ,ણ્ચ,ણ્છ,ણ્જ,ણ્ઝ,ણ્ઞ,ણ્ટ,ણ્ઠ,ણ્ડ,ણ્ઢ,ણ્ણ,ણ્ત,ણ્થ,ણ્દ,ણ્ધ,ણ્ન,ણ્પ,ણ્ફ,ણ્બ,ણ્ભ,ણ્મ,ણ્ય,ણ્ર,ણ્લ,ણ્ળ,ણ્વ,ણ્શ,ણ્ષ,ણ્સ,ણ્હ"

let KEYth = "થ્ક,થ્ખ,થ્ગ,થ્ઘ,થ્ઙ,થ્ચ,થ્છ,થ્જ,થ્ઝ,થ્ઞ,થ્ટ,થ્ઠ,થ્ડ,થ્ઢ,થ્ણ,થ્ત,થ્થ,થ્દ,થ્ધ,થ્ન,થ્પ,થ્ફ,થ્બ,થ્ભ,થ્મ,થ્ય,થ્ર,થ્લ,થ્ળ,થ્વ,થ્શ,થ્ષ,થ્સ,થ્હ"

let KEYdh = "થ્ધ્ક,ધ્ખ,ધ્ગ,ધ્ઘ,ધ્ઙ,ધ્ચ,ધ્છ,ધ્જ,ધ્ઝ,ધ્ઞ,ધ્ટ,ધ્ઠ,ધ્ડ,ધ્ઢ,ધ્ણ,ધ્ત,ધ્થ,ધ્દ,ધ્ધ,ધ્ન,ધ્પ,ધ્ફ,ધ્બ,ધ્ભ,ધ્મ,ધ્ય,ધ્ર,ધ્લ,ધ્ળ,ધ્વ,ધ્શ,ધ્ષ,ધ્સ,ધ્હ"

let KEYp = "પ્ક,પ્ખ,પ્ગ,પ્ઘ,પ્ઙ,પ્ચ,પ્છ,પ્જ,પ્ઝ,પ્ઞ,પ્ટ,પ્ઠ,પ્ડ,પ્ઢ,પ્ણ,પ્ત,પ્થ,પ્દ,પ્ધ,પ્ન,પ્પ,પ્ફ,પ્બ,પ્ભ,પ્મ,પ્ય,પ્ર,પ્લ,પ્ળ,પ્વ,પ્શ,પ્ષ,પ્સ,પ્હ"

let KEYb = "બ્ક,બ્ખ,બ્ગ,બ્ઘ,બ્ઙ,બ્ચ,બ્છ,બ્જ,બ્ઝ,બ્ઞ,બ્ટ,બ્ઠ,બ્ડ,બ્ઢ,બ્ણ,બ્ત,બ્થ,બ્દ,બ્ધ,બ્ન,બ્પ,બ્ફ,બ્બ,બ્ભ,બ્મ,બ્ય,બ્ર,બ્લ,બ્ળ,બ્વ,બ્શ,બ્ષ,બ્સ,બ્હ"

let KEYm = "મ્ક,મ્ખ,મ્ગ,મ્ઘ,મ્ઙ,મ્ચ,મ્છ,મ્જ,મ્ઝ,મ્ઞ,મ્ટ,મ્ઠ,મ્ડ,મ્ઢ,મ્ણ,મ્ત,મ્થ,મ્દ,મ્ધ,મ્ન,મ્પ,મ્ફ,મ્બ,મ્ભ,મ્મ,મ્ય,મ્ર,મ્લ,મ્ળ,મ્વ,મ્શ,મ્ષ,મ્સ,મ્હ"

let KEYr = "ર્ક,ર્ખ,ર્ગ,ર્ઘ,ર્ઙ,ર્ચ,ર્છ,ર્જ,ર્ઝ,ર્ઞ,ર્ટ,ર્ઠ,ર્ડ,ર્ઢ,ર્ણ,ર્ત,ર્થ,ર્દ,ર્ધ,ર્ન,ર્પ,ર્ફ,ર્બ,ર્ભ,ર્મ,ર્ય,ર્ર,ર્લ,ર્ળ,ર્વ,ર્શ,ર્ષ,ર્સ,ર્હ"

let KEYl = "ર્ળ્ક,ળ્ખ,ળ્ગ,ળ્ઘ,ળ્ઙ,ળ્ચ,ળ્છ,ળ્જ,ળ્ઝ,ળ્ઞ,ળ્ટ,ળ્ઠ,ળ્ડ,ળ્ઢ,ળ્ણ,ળ્ત,ળ્થ,ળ્દ,ળ્ધ,ળ્ન,ળ્પ,ળ્ફ,ળ્બ,ળ્ભ,ળ્મ,ળ્ય,ળ્ર,ળ્લ,ળ્ળ,ળ્વ,ળ્શ,ળ્ષ,ળ્સ,ળ્હ"

let KEYsh = "ર્શ્ક,શ્ખ,શ્ગ,શ્ઘ,શ્ઙ,શ્ચ,શ્છ,શ્જ,શ્ઝ,શ્ઞ,શ્ટ,શ્ઠ,શ્ડ,શ્ઢ,શ્ણ,શ્ત,શ્થ,શ્દ,શ્ધ,શ્ન,શ્પ,શ્ફ,શ્બ,શ્ભ,શ્મ,શ્ય,શ્ર,શ્લ,શ્ળ,શ્વ,શ્શ,શ્ષ,શ્સ,શ્હ"

let KEYs = "ર્સ્ક,સ્ખ,સ્ગ,સ્ઘ,સ્ઙ,સ્ચ,સ્છ,સ્જ,સ્ઝ,સ્ઞ,સ્ટ,સ્ઠ,સ્ડ,સ્ઢ,સ્ણ,સ્ત,સ્થ,સ્દ,સ્ધ,સ્ન,સ્પ,સ્ફ,સ્બ,સ્ભ,સ્મ,સ્ય,સ્ર,સ્લ,સ્ળ,સ્વ,સ્શ,સ્ષ,સ્સ,સ્હ"

let KEYkh = "ર્ખ્ક,ખ્ખ,ખ્ગ,ખ્ઘ,ખ્ઙ,ખ્ચ,ખ્છ,ખ્જ,ખ્ઝ,ખ્ઞ,ખ્ટ,ખ્ઠ,ખ્ડ,ખ્ઢ,ખ્ણ,ખ્ત,ખ્થ,ખ્દ,ખ્ધ,ખ્ન,ખ્પ,ખ્ફ,ખ્બ,ખ્ભ,ખ્મ,ખ્ય,ખ્ર,ખ્લ,ખ્ળ,ખ્વ,ખ્શ,ખ્ષ,ખ્સ,ખ્હ"

let KEYgh = "ઘ્ક,ઘ્ખ,ઘ્ગ,ઘ્ઘ,ઘ્ઙ,ઘ્ચ,ઘ્છ,ઘ્જ,ઘ્ઝ,ઘ્ઞ,ઘ્ટ,ઘ્ઠ,ઘ્ડ,ઘ્ઢ,ઘ્ણ,ઘ્ત,ઘ્થ,ઘ્દ,ઘ્ધ,ઘ્ન,ઘ્પ,ઘ્ફ,ઘ્બ,ઘ્ભ,ઘ્મ,ઘ્ય,ઘ્ર,ઘ્લ,ઘ્ળ,ઘ્વ,ઘ્શ,ઘ્ષ,ઘ્સ,ઘ્હ"

let KEYch = "ચ્ક,ચ્ખ,ચ્ગ,ચ્ઘ,ચ્ઙ,ચ્ચ,ચ્છ,ચ્જ,ચ્ઝ,ચ્ઞ,ચ્ટ,ચ્ઠ,ચ્ડ,ચ્ઢ,ચ્ણ,ચ્ત,ચ્થ,ચ્દ,ચ્ધ,ચ્ન,ચ્પ,ચ્ફ,ચ્બ,ચ્ભ,ચ્મ,ચ્ય,ચ્ર,ચ્લ,ચ્ળ,ચ્વ,ચ્શ,ચ્ષ,ચ્સ,ચ્હ"

let KEYj = "જ્ક,જ્ખ,જ્ગ,જ્ઘ,જ્ઙ,જ્ચ,જ્છ,જ્જ,જ્ઝ,જ્ઞ,જ્ટ,જ્ઠ,જ્ડ,જ્ઢ,જ્ણ,જ્ત,જ્થ,જ્દ,જ્ધ,જ્ન,જ્પ,જ્ફ,જ્બ,જ્ભ,જ્મ,જ્ય,જ્ર,જ્લ,જ્ળ,જ્વ,જ્શ,જ્ષ,જ્સ,જ્હ"


let KEYza = "ઞ્ક,ઞ્ખ,ઞ્ગ,ઞ્ઘ,ઞ્ઙ,ઞ્ચ,ઞ્છ,ઞ્જ,ઞ્ઝ,ઞ્ઞ,ઞ્ટ,ઞ્ઠ,ઞ્ડ,ઞ્ઢ,ઞ્ણ,ઞ્ત,ઞ્થ,ઞ્દ,ઞ્ધ,ઞ્ન,ઞ્પ,ઞ્ફ,ઞ્બ,ઞ્ભ,ઞ્મ,ઞ્ય,ઞ્ર,ઞ્લ,ઞ્ળ,ઞ્વ,ઞ્શ,ઞ્ષ,ઞ્સ,ઞ્હ"

let KEYtha = "ઠ્ક,ઠ્ખ,ઠ્ગ,ઠ્ઘ,ઠ્ઙ,ઠ્ચ,ઠ્છ,ઠ્જ,ઠ્ઝ,ઠ્ઞ,ઠ્ટ,ઠ્ઠ,ઠ્ડ,ઠ્ઢ,ઠ્ણ,ઠ્ત,ઠ્થ,ઠ્દ,ઠ્ધ,ઠ્ન,ઠ્પ,ઠ્ફ,ઠ્બ,ઠ્ભ,ઠ્મ,ઠ્ય,ઠ્ર,ઠ્લ,ઠ્ળ,ઠ્વ,ઠ્શ,ઠ્ષ,ઠ્સ,ઠ્હ"

let KEYdha = "ઢ્ક,ઢ્ખ,ઢ્ગ,ઢ્ઘ,ઢ્ઙ,ઢ્ચ,ઢ્છ,ઢ્જ,ઢ્ઝ,ઢ્ઞ,ઢ્ટ,ઢ્ઠ,ઢ્ડ,ઢ્ઢ,ઢ્ણ,ઢ્ત,ઢ્થ,ઢ્દ,ઢ્ધ,ઢ્ન,ઢ્પ,ઢ્ફ,ઢ્બ,ઢ્ભ,ઢ્મ,ઢ્ય,ઢ્ર,ઢ્લ,ઢ્ળ,ઢ્વ,ઢ્શ,ઢ્ષ,ઢ્સ,ઢ્હ"

let KEYt = "ત્ક,ત્ખ,ત્ગ,ત્ઘ,ત્ઙ,ત્ચ,ત્છ,ત્જ,ત્ઝ,ત્ઞ,ત્ટ,ત્ઠ,ત્ડ,ત્ઢ,ત્ણ,ત્ત,ત્થ,ત્દ,ત્ધ,ત્ન,ત્પ,ત્ફ,ત્બ,ત્ભ,ત્મ,ત્ય,ત્ર,ત્લ,ત્ળ,ત્વ,ત્શ,ત્ષ,ત્સ,ત્હ"

let KEYda = "દ્ક,દ્ખ,દ્ગ,દ્ઘ,દ્ઙ,દ્ચ,દ્છ,દ્જ,દ્ઝ,દ્ઞ,દ્ટ,દ્ઠ,દ્ડ,દ્ઢ,દ્ણ,દ્ત,દ્થ,દ્દ,દ્ધ,દ્ન,દ્પ,દ્ફ,દ્બ,દ્ભ,દ્મ,દ્ય,દ્ર,દ્લ,દ્ળ,દ્વ,દ્શ,દ્ષ,દ્સ,દ્હ"

let KEYn = "ન્ક,ન્ખ,ન્ગ,ન્ઘ,ન્ઙ,ન્ચ,ન્છ,ન્જ,ન્ઝ,ન્ઞ,ન્ટ,ન્ઠ,ન્ડ,ન્ઢ,ન્ણ,ન્ત,ન્થ,ન્દ,ન્ધ,ન્ન,ન્પ,ન્ફ,ન્બ,ન્ભ,ન્મ,ન્ય,ન્ર,ન્લ,ન્ળ,ન્વ,ન્શ,ન્ષ,ન્સ,ન્હ"

let KEYph = "ફ્ક,ફ્ખ,ફ્ગ,ફ્ઘ,ફ્ઙ,ફ્ચ,ફ્છ,ફ્જ,ફ્ઝ,ફ્ઞ,ફ્ટ,ફ્ઠ,ફ્ડ,ફ્ઢ,ફ્ણ,ફ્ત,ફ્થ,ફ્દ,ફ્ધ,ફ્ન,ફ્પ,ફ્ફ,ફ્બ,ફ્ભ,ફ્મ,ફ્ય,ફ્ર,ફ્લ,ફ્ળ,ફ્વ,ફ્શ,ફ્ષ,ફ્સ,ફ્હ"

let KEYbh = "ભ્ક,ભ્ખ,ભ્ગ,ભ્ઘ,ભ્ઙ,ભ્ચ,ભ્છ,ભ્જ,ભ્ઝ,ભ્ઞ,ભ્ટ,ભ્ઠ,ભ્ડ,ભ્ઢ,ભ્ણ,ભ્ત,ભ્થ,ભ્દ,ભ્ધ,ભ્ન,ભ્પ,ભ્ફ,ભ્બ,ભ્ભ,ભ્મ,ભ્ય,ભ્ર,ભ્લ,ભ્ળ,ભ્વ,ભ્શ,ભ્ષ,ભ્સ,ભ્હ"

let KEYy = "ય્ક,ય્ખ,ય્ગ,ય્ઘ,ય્ઙ,ય્ચ,ય્છ,ય્જ,ય્ઝ,ય્ઞ,ય્ટ,ય્ઠ,ય્ડ,ય્ઢ,ય્ણ,ય્ત,ય્થ,ય્દ,ય્ધ,ય્ન,ય્પ,ય્ફ,ય્બ,ય્ભ,ય્મ,ય્ય,ય્ર,ય્લ,ય્ળ,ય્વ,ય્શ,ય્ષ,ય્સ,ય્હ"

let KEYla = "લ્ક,લ્ખ,લ્ગ,લ્ઘ,લ્ઙ,લ્ચ,લ્છ,લ્જ,લ્ઝ,લ્ઞ,લ્ટ,લ્ઠ,લ્ડ,લ્ઢ,લ્ણ,લ્ત,લ્થ,લ્દ,લ્ધ,લ્ન,લ્પ,લ્ફ,લ્બ,લ્ભ,લ્મ,લ્ય,લ્ર,લ્લ,લ્ળ,લ્વ,લ્શ,લ્ષ,લ્સ,લ્હ"

let KEYv = "વ્ક,વ્ખ,વ્ગ,વ્ઘ,વ્ઙ,વ્ચ,વ્છ,વ્જ,વ્ઝ,વ્ઞ,વ્ટ,વ્ઠ,વ્ડ,વ્ઢ,વ્ણ,વ્ત,વ્થ,વ્દ,વ્ધ,વ્ન,વ્પ,વ્ફ,વ્બ,વ્ભ,વ્મ,વ્ય,વ્ર,વ્લ,વ્ળ,વ્વ,વ્શ,વ્ષ,વ્સ,વ્હ"

let KEYsha = "ષ્ક,ષ્ખ,ષ્ગ,ષ્ઘ,ષ્ઙ,ષ્ચ,ષ્છ,ષ્જ,ષ્ઝ,ષ્ઞ,ષ્ટ,ષ્ઠ,ષ્ડ,ષ્ઢ,ષ્ણ,ષ્ત,ષ્થ,ષ્દ,ષ્ધ,ષ્ન,ષ્પ,ષ્ફ,ષ્બ,ષ્ભ,ષ્મ,ષ્ય,ષ્ર,ષ્લ,ષ્ળ,ષ્વ,ષ્શ,ષ્ષ,ષ્સ,ષ્હ"

let KEYh = "હ્ક,હ્ખ,હ્ગ,હ્ઘ,હ્ઙ,હ્ચ,હ્છ,હ્જ,હ્ઝ,હ્ઞ,હ્ટ,હ્ઠ,હ્ડ,હ્ઢ,હ્ણ,હ્ત,હ્થ,હ્દ,હ્ધ,હ્ન,હ્પ,હ્ફ,હ્બ,હ્ભ,હ્મ,હ્ય,હ્ર,હ્લ,હ્ળ,હ્વ,હ્શ,હ્ષ,હ્સ,હ્હ"

class ViewController: BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource,GADBannerViewDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate
{
    @IBOutlet var btnMenu : UIButton!
    var arrData : Array<Dictionary<String,AnyObject>> = Array()
    @IBOutlet var collectionView : UICollectionView!
    @IBOutlet var tblViewSearchObj : UITableView!
    @IBOutlet var txtsearchBox : customTextField!
    var arrStoreFilterData : Array<Dictionary<String,AnyObject>> = Array()
    var arrStoreHalfCharacter : Array<String> = Array()
    var arrStoreOriginalData : Array<String> = Array()
    var arrMainKey : Array<Dictionary<String,AnyObject>> = Array()
    var arrMutSearchingArrayCountry : Array<Dictionary<String,AnyObject>> = Array()
    var strStoreEnglishWord : String!
    var customKeyboard: PMCustomKeyboard?
    var pkcustomKeyboard: PKCustomKeyboard?
    var keyboardSize = CGSize.zero
    var recognizer : UITapGestureRecognizer!
    var strStoreTextField : String!
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblType : UILabel!
    @IBOutlet var lblMeaning : UILabel!
    @IBOutlet var headerHeight : NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.activity.stopAnimating()
//        self.activity.isHidden = true
//        self.activity.hidesWhenStopped = true

        if UI_USER_INTERFACE_IDIOM() == .phone {
            customKeyboard = PMCustomKeyboard()
            customKeyboard?.textView = txtsearchBox
        }
        else {
            pkcustomKeyboard = PKCustomKeyboard()
            pkcustomKeyboard?.textView = txtsearchBox
        }
        if Display.typeIsLike == DisplayType.iphoneX
        {
            headerHeight.constant = 84
        }
        else
        {
            headerHeight.constant = 64
        }
        
        
        let userDefaults = UserDefaults.standard
        
        if !userDefaults.bool(forKey: "walkthroughPresentedForDetail")
        {
            showWalkThrought()
            
            userDefaults.set(true, forKey: "walkthroughPresentedForDetail")
            userDefaults.synchronize()
        }
        else
        {
            let bannerView = GADBannerView.init(frame: CGRect.init(x: 0, y: self.view.frame.height - 50, width: self.view.frame.width, height: 50))
            
            self.view.addSubview(bannerView)
            bannerView.adUnitID = ADS_UNIT_ID
            bannerView.rootViewController = self
            bannerView.delegate = self
            
            bannerView.load(GADRequest())
        }

        btnMenu.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        
        let strKeyWord: String = "ક,ખ,ગ,ઘ,ઙ,ચ,છ,જ,ઝ,ઞ,ટ,ઠ,ડ,ઢ,ણ,ત,થ,દ,ધ,ન,પ,ફ,બ,ભ,મ,ય,ર,લ,ળ,વ,શ,ષ,સ,હ"
        let arrkey: Array<String> = strKeyWord.components(separatedBy: ",")
        
        let arrAllKey: Array<String> = [KEYk, KEYkh, KEYg, KEYgh, KEYdah, KEYch, KEYchh, KEYj, KEYz, KEYza, KEYta, KEYtha, KEYd, KEYdha, KEYna, KEYt, KEYth, KEYda, KEYdh, KEYn, KEYp, KEYph, KEYb, KEYbh, KEYm, KEYy, KEYr, KEYla, KEYl, KEYv, KEYsh, KEYsha, KEYs, KEYh]
        
        arrMainKey = generateFormatparent(arrkey, andchild: arrAllKey) as! Array<Dictionary<String, AnyObject>>
        
        //  Converted with Swiftify v1.0.6423 - https://objectivec2swift.com/
        arrStoreHalfCharacter = Array()
        arrStoreOriginalData = Array()
        
        tblViewSearchObj.isHidden = true
        tblViewSearchObj.separatorStyle = .none
        txtsearchBox.addTarget(self, action: #selector(self.textFieldDidChange), for: .editingChanged)
        

        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        arrData = DBOperation.selectData(SELECT_DB_QUERY) as! Array<Dictionary<String, AnyObject>>

        arrMutSearchingArrayCountry = arrData
        
        lblTitle.text = "\(arrData[0]["word"]!)"
        lblType.text = "પ્રકાર : \(arrData[0]["type"]!)"
        lblMeaning.text = "\(arrData[0]["meaning"]!)"
    }
    func showWalkThrought()
    {
        let walkView = UIView.init(frame: self.view.frame)
        walkView.backgroundColor = UIColor.black
        self.view.addSubview(walkView)
        
        let imgView = UIImageView.init(frame: walkView.frame)
        walkView.addSubview(imgView)
        imgView.image = UIImage.init(named: "Home_Coatch")
        
        let btnGotIt = UIButton.init(frame: CGRect.init(x: 30, y: self.view.frame.height - 80, width: self.view.frame.width - 60, height: 40))
        btnGotIt.setTitle("Got It", for: .normal)
        btnGotIt.titleLabel?.font = UIFont.init(name: "Poppins-Regular", size: 17)
        btnGotIt.titleLabel?.textColor = UIColor.white
        btnGotIt.layer.borderColor = UIColor.white.cgColor
        btnGotIt.layer.borderWidth = 1.0
        btnGotIt.layer.cornerRadius = 5.0
        btnGotIt.addTarget(self, action: #selector(btnGotItPress(sender:)), for: .touchUpInside)
        walkView.addSubview(btnGotIt)
        self.view.bringSubview(toFront: walkView)
    }
    @IBAction func btnGotItPress(sender:UIButton)
    {
        let userDefaults = UserDefaults.standard
        userDefaults.set(true, forKey: "walkthroughPresentedForDetail")
        userDefaults.synchronize()
        sender.superview?.removeFromSuperview()
        
        let bannerView = GADBannerView.init(frame: CGRect.init(x: 0, y: self.view.frame.height - 50, width: self.view.frame.width, height: 50))
        
        self.view.addSubview(bannerView)
        bannerView.adUnitID = ADS_UNIT_ID
        bannerView.rootViewController = self
        bannerView.delegate = self
        
        bannerView.load(GADRequest())
    }

    func generateFormatparent(_ arrayPar: Array<String>, andchild arrayChild: Array<String>) -> [Any] {
        var arrAllFinalKey = [Any]()
        for i in 0..<arrayPar.count {
            let arrSubKey = arrayChild[i].components(separatedBy: ",")
            var arrFinalSubkey = [Any]()
            for j in 0..<arrSubKey.count {
                var dictKey = [AnyHashable: Any]()
                dictKey[arrayPar[j]] = arrSubKey[j]
                arrFinalSubkey.append(dictKey)
            }
            var dictFinalKey = [AnyHashable: Any]()
            dictFinalKey[arrayPar[i]] = arrFinalSubkey
            arrAllFinalKey.append(dictFinalKey)
        }
        return arrAllFinalKey
    }
    func storeData(inArray str: String) -> Array<Dictionary<String, AnyObject>> {
        let predicate_convers = NSPredicate(format: "word beginswith[c] %@", str)
        let arr: Array<Dictionary<String, AnyObject>> = arrMutSearchingArrayCountry.filter { predicate_convers.evaluate(with: $0) } as Array<Dictionary<String, AnyObject>>
        return arr
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.lightContent, animated: true)

    }
    func returnStringfirst(_ strfirst: String, andLast strLast: String) -> String {
        var arr = Array<Dictionary<String,Any>>()
        for i in 0..<arrMainKey.count {
            let dict2 = arrMainKey[i]
            
            if "\(dict2.keys.first!)" == strfirst {
                arr = dict2[strfirst] as! Array<Dictionary<String, Any>>
            }
        }
        var str: String = ""
        for i in 0..<arr.count {
            var dict2 = arr[i]
            if ("\(dict2.keys.first!)" == strLast) {
                str = dict2[strLast] as! String
            }
        }

        if str == nil
        {
            str = txtsearchBox.text!
        }
        return str
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if range.length == 1 && (string as NSString).length == 0 {
            let newString: String! = (strStoreEnglishWord as NSString).substring(to: (strStoreEnglishWord as NSString).length - 1)
            strStoreEnglishWord = newString!
        }
        else {
            strStoreEnglishWord = strStoreEnglishWord + (string)
        }
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
       
        arrStoreHalfCharacter.removeAll()
        arrStoreOriginalData.removeAll()
        arrStoreFilterData.removeAll()
      //  textField.resignFirstResponder()
        arrStoreFilterData.removeAll()
        arrMutSearchingArrayCountry = arrData
        
        lblTitle.text = "\(arrData[0]["word"]!)"
        lblType.text = "પ્રકાર : \(arrData[0]["type"]!)"
        lblMeaning.text = "\(arrData[0]["meaning"]!)"
        collectionView.reloadData()
        collectionView.reloadInputViews()
        collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)

        return true
    }
    
    @IBAction func btnSearchPressed(_ sender: Any) {
        txtsearchBox.resignFirstResponder()
        self.view.addSubview(activity)
        activity.startAnimating()
        activity.hidesWhenStopped = true
        
        //  lblTitle.text = "\(arrStoreFilterData[indexPath.row]["word"]!)"
        // txtsearchBox.text = "\(arrStoreFilterData[indexPath.row]["word"]!)"
        //   lblType.text = "પ્રકાર : \(arrStoreFilterData[indexPath.row]["type"]!)"
        // lblMeaning.text = "\(arrStoreFilterData[indexPath.row]["meaning"]!)"
        var boolForCheck : Bool = false
        DispatchQueue.global().async {
            for i in 0 ..< self.arrData.count
            {
                let dict = self.arrData[i] as [String:AnyObject]
                if "\(dict["word"]!)" == self.txtsearchBox.text
                {
                    if let indexObj = self.arrData.index(where: { (myObject) -> Bool in
                        return "\(String(describing: myObject["word"]))" == "\(String(describing: dict["word"]))"
                    })
                    {
                        print(indexObj)
                        
                       self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1, execute: {
                            self.lblTitle.text = "\(self.arrData[i]["word"]!)"
                            self.lblType.text = "પ્રકાર : \(self.arrData[i]["type"]!)"
                            self.lblMeaning.text = "\(self.arrData[i]["meaning"]!)"
                            self.collectionView.reloadData()
                            self.collectionView.reloadInputViews()
                            self.tblViewSearchObj.isHidden = true
                            self.activity.isHidden = true
                            self.activity.stopAnimating()
                        })
                        
                        boolForCheck = true
                        
                        return
                    }
                    //                index = arrData.indexOf(dict)
                }else{
                    boolForCheck = false
                    
                }
                
            }
            if(boolForCheck){
                boolForCheck = false
            }else{
                boolForCheck = false
                
                if(self.txtsearchBox.text?.characters.count == 0){
                    
                }else{
                    let alert = UIAlertController(title: "Alert", message: "Not Found", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Try Another", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)

                }
                
            }
            DispatchQueue.main.async {
                
                self.tblViewSearchObj.isHidden = true
                
                self.activity.isHidden = true
                self.activity.stopAnimating()
            }
        }

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        txtsearchBox.resignFirstResponder()
        self.view.addSubview(activity)
        activity.startAnimating()
        activity.hidesWhenStopped = true

      //  lblTitle.text = "\(arrStoreFilterData[indexPath.row]["word"]!)"
       // txtsearchBox.text = "\(arrStoreFilterData[indexPath.row]["word"]!)"
     //   lblType.text = "પ્રકાર : \(arrStoreFilterData[indexPath.row]["type"]!)"
       // lblMeaning.text = "\(arrStoreFilterData[indexPath.row]["meaning"]!)"
        var boolForCheck : Bool = false
        DispatchQueue.global().async {
            for i in 0 ..< self.arrData.count
            {
                let dict = self.arrData[i] as [String:AnyObject]
                if "\(dict["word"]!)" == self.txtsearchBox.text
                {
                    if let indexObj = self.arrData.index(where: { (myObject) -> Bool in
                        return "\(String(describing: myObject["word"]))" == "\(String(describing: dict["word"]))"
                    })
                    {
                        print(indexObj)
                        

                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+2, execute: {
                            self.lblTitle.text = "\(self.arrData[i]["word"]!)"
                            self.lblType.text = "પ્રકાર : \(self.arrData[i]["type"]!)"
                            self.lblMeaning.text = "\(self.arrData[i]["meaning"]!)"
                            self.collectionView.reloadData()
                            self.collectionView.reloadInputViews()
                            self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)

                            self.tblViewSearchObj.isHidden = true
                            self.activity.isHidden = true
                            self.activity.stopAnimating()
                        })
                        
                        boolForCheck = true

                        return
                    }
                    //                index = arrData.indexOf(dict)
                }else{
                    boolForCheck = false
                    
                }
                
            }
            if(boolForCheck){
                boolForCheck = false
            }else{
                boolForCheck = false

                if(self.txtsearchBox.text?.characters.count == 0){
                    
                }else{
                    let alert = UIAlertController(title: "Alert", message: "Not Found", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Try Another", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                }

            }
            DispatchQueue.main.async {
                
                self.tblViewSearchObj.isHidden = true

                self.activity.isHidden = true
                self.activity.stopAnimating()
            }
        }

        return true
    }
    
    // MARK: - TextField value Change Method
    @objc func textFieldDidChange(_ txtValue: UITextField)
    {
        if (txtsearchBox.text?.characters.count)! <= 0
        {
            textFieldShouldClear(txtValue)
        }
        if (txtValue.text == "અૈ")
        {
            txtValue.text = "ઐ"
            txtsearchBox.text = "ઐ"
        }
        if (txtValue.text == "અૅા") || (txtValue.text == "અાૅ") || (txtValue.text == "આૅ")
        {
            txtValue.text = "ઑ"
            txtsearchBox.text = "ઑ"
        }
        if (txtValue.text == "આૈ") || (txtValue.text == "અૌ") || (txtValue.text == "ઐા") || (txtValue.text == "ઐા")
        {
            txtValue.text = "ઔ"
            txtsearchBox.text = "ઔ"
        }
        if (txtValue.text == "અા") {
            txtValue.text = "આ"
            txtsearchBox.text = "આ"
        }
        if (txtValue.text == "અે") {
            txtValue.text = "એ"
            txtsearchBox.text = "એ"
        }
        if (txtValue.text == "આે") || (txtValue.text == "એા") || (txtValue.text == "અો") {
            txtValue.text = "ઓ"
            txtsearchBox.text = "ઓ"
        }
        if (txtValue.text == "અૅ") {
            txtValue.text = "ઍ"
            txtsearchBox.text = "ઍ"
        }
        
        var code: String = ""
        if (txtsearchBox.text?.characters.count)! >= 2 {
            let index = txtsearchBox.text!.index((txtsearchBox.text?.startIndex)!, offsetBy: 2, limitedBy: (txtsearchBox.text?.endIndex)!)
            
            code = (txtsearchBox.text?.substring(from: index!))!
        }
        if (code == "ાૅ") || (code == "ૅા") {
            code = "ૉ"
            let index = txtsearchBox.text!.index((txtsearchBox.text?.startIndex)!, offsetBy: 2, limitedBy: (txtsearchBox.text?.endIndex)!)
            
            txtsearchBox.text = (txtsearchBox.text?.substring(from: index!))!
            txtsearchBox.text = txtsearchBox.text! + (code)
        }
        if (code == "ેા") || (code == "ાે") {
            code = "ો"
            let index = txtsearchBox.text!.index((txtsearchBox.text?.startIndex)!, offsetBy: 2, limitedBy: (txtsearchBox.text?.endIndex)!)
            
            txtsearchBox.text = (txtsearchBox.text?.substring(from: index!))!
            txtsearchBox.text = txtsearchBox.text! + (code)
        }
        strStoreTextField = txtsearchBox.text
        var str: String! = (strStoreTextField as NSString).substring(to: (strStoreTextField as NSString).length)
        var firstLetter: String = ""
        if (str?.characters.count ?? 0) == 0 {
            str = strStoreTextField
        }
        else {
            firstLetter = (txtsearchBox.text! as NSString).substring(to: 1)
        }
        //////////////////////////////////////////////////////
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            if (pkcustomKeyboard?.flagdeleteOrnot)! {
                if arrStoreOriginalData.count > 0 {
                    arrStoreOriginalData.removeLast()
                    txtsearchBox.text = ""
                    for i in 0..<arrStoreOriginalData.count {
                        txtsearchBox.text = txtsearchBox.text! + (arrStoreOriginalData[i])
                    }
                }
                else if arrStoreOriginalData.count == 0 {
                    arrStoreHalfCharacter.removeAll()
                    arrStoreOriginalData.removeAll()
                    tblViewSearchObj.isHidden = true
                }
            }
        }
        else {
            if (customKeyboard?.flagdeleteOrnot)! {
                if arrStoreOriginalData.count > 0 {
                    arrStoreOriginalData.removeLast()
                    txtsearchBox.text = ""
                    for i in 0..<arrStoreOriginalData.count {
                        txtsearchBox.text = txtsearchBox.text! + (arrStoreOriginalData[i])
                    }
                }
                else if arrStoreOriginalData.count == 0 {
                    arrStoreHalfCharacter.removeAll()
                    arrStoreOriginalData.removeAll()
                    tblViewSearchObj.isHidden = true
                }
            }
        }
        
        
        tblViewSearchObj.isHidden=true;
        
        if !(txtValue.text! == "") || txtValue.text!.characters.count != 0
        {
            //////////////////
            var strLastChar: String! = ""
            if UIDevice.current.userInterfaceIdiom == .pad {
                strLastChar = (pkcustomKeyboard?.character)!
            }
            else {
                strLastChar = (customKeyboard?.character)!
            }
            
            if arrStoreHalfCharacter.count == 0
            {
                if (strLastChar == "੍") {
                    if (txtValue.text!.characters.count) >= 2
                    {
                        print("-------------------")
                        print(arrStoreHalfCharacter)
                        print(txtValue.text!)
                        print((txtValue.text! as NSString).length)
                        arrStoreHalfCharacter.append((txtValue.text! as NSString).substring(with: NSMakeRange((txtValue.text! as NSString).length - 2, 1)))
                        arrStoreHalfCharacter.append(strLastChar)
                        print("-------------------")
                        print(arrStoreHalfCharacter)
                        print(txtValue.text!)
                        print((txtValue.text! as NSString).length)
                    }
                }
            }
            else if arrStoreHalfCharacter.count == 2
            {
                if ("\(arrStoreHalfCharacter[1])" == strLastChar) {
                    arrStoreHalfCharacter.removeAll()
                }
                else
                {
                    let strCharLength: String! = (txtValue.text! as NSString).substring(to: (txtValue.text! as NSString).length - 1)
                    let sampleText: String! = txtValue.text!
                    let pattern: String! = strCharLength!
                    let expression = try? NSRegularExpression(pattern: pattern!, options: [])
                    
                    
                    let range = NSRange(location: 0, length: (sampleText! as NSString).length)
                    
                    expression!.enumerateMatches(in: sampleText!, options: [], range: range, using: { (result:NSTextCheckingResult?, flags:NSRegularExpression.MatchingFlags, stop:UnsafeMutablePointer<ObjCBool>) in
                        
                        let strSearchValue: String! = self.returnStringfirst("\(arrStoreHalfCharacter[0])", andLast: strLastChar)
                        let s: String = (sampleText! as NSString).substring(with: NSRange(location: (sampleText! as NSString).length - 3, length: 3))
                        let strreplacestring: String! = sampleText!.replacingOccurrences(of: s, with: strSearchValue!)
                        txtsearchBox.text = strreplacestring
                        arrStoreHalfCharacter.removeAll()
                    })
                    str = txtsearchBox.text!
                    firstLetter = txtsearchBox.text!
                }
            }
            var filteredArray_convers = Array<Dictionary<String,AnyObject>>()
            if !(txtsearchBox.text == "") {
                filteredArray_convers = DBOperation.getSearchData(txtsearchBox.text) as! Array<Dictionary<String,AnyObject>>
            }
            if filteredArray_convers.count > 0 {
                arrStoreFilterData.removeAll()
                arrStoreFilterData = arrStoreFilterData + filteredArray_convers
                tblViewSearchObj.isHidden = false
                self.view.removeGestureRecognizer(self.recognizer)

                tblViewSearchObj.contentOffset = CGPoint(x: 0, y: 0)
            }
            if filteredArray_convers.count == 0 {
                arrStoreFilterData.removeAll()
            }
            if (str as NSString).length == 0 {
                arrStoreFilterData.removeAll()
                arrStoreFilterData = arrStoreFilterData + arrMutSearchingArrayCountry
                tblViewSearchObj.reloadData()
            }
            tblViewSearchObj.reloadData()
        }
        else
        {
            tblViewSearchObj.isHidden = true
        }
    }
    // MARK: - TableView Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrStoreFilterData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell? = tblViewSearchObj.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell?.textLabel?.text = "\(arrStoreFilterData[indexPath.row]["word"]!)"
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        txtsearchBox.resignFirstResponder()
        self.tblViewSearchObj.addSubview(activity)
        self.view.addSubview(activity)
        activity.startAnimating()
        activity.hidesWhenStopped = true
        
        
        lblTitle.text = "\(arrStoreFilterData[indexPath.row]["word"]!)"
        txtsearchBox.text = "\(arrStoreFilterData[indexPath.row]["word"]!)"

        lblType.text = "પ્રકાર : \(arrStoreFilterData[indexPath.row]["type"]!)"
        lblMeaning.text = "\(arrStoreFilterData[indexPath.row]["meaning"]!)"

        DispatchQueue.global().async {
            for i in 0 ..< self.arrData.count
            {
                let dict = self.arrData[i] as [String:AnyObject]
                if "\(dict["word"]!)" == "\(self.arrStoreFilterData[indexPath.row]["word"]!)"
                {
                    if let indexObj = self.arrData.index(where: { (myObject) -> Bool in
                        return "\(String(describing: myObject["word"]))" == "\(String(describing: dict["word"]))"
                    })
                    {
                        print(indexObj)
                        

                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1, execute: {
                            self.tblViewSearchObj.isHidden = true
                            self.collectionView.reloadData()
                            self.collectionView.reloadInputViews()
                            self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)

                            self.activity.isHidden = true
                            self.activity.stopAnimating()
                        })
                        
                        
                        return
                    }
                    //                index = arrData.indexOf(dict)
                }
                
                
            }
            DispatchQueue.main.async {
                self.tblViewSearchObj.isHidden = true
                self.activity.isHidden = true
                self.activity.stopAnimating()
            }
        }
        
    }
    @IBAction func btnLeftPress(sender:UIButton)
    {
        collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
    }
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if(arrStoreFilterData.count == 0){

            return arrData.count
            
        }else{
            if(arrStoreFilterData.count > 50 ){
                return 50

            }else{
                return arrStoreFilterData.count

            }

        }
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        return CGSize(width: collectionView.frame.width/2.5, height: 200);
        return CGSize(width: collectionView.frame.width/2, height: 120/160 * (collectionView.frame.width/2));
        
    }
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! MyCollectionViewCell
        if(arrStoreFilterData.count == 0){
            cell.bgImg.layer.cornerRadius = 10.0
            cell.lblTitle.text = "\(arrData[indexPath.row]["word"]!)"
            cell.lblType.text = "પ્રકાર : \(arrData[indexPath.row]["type"]!)"

        }else{
            if(indexPath.row < arrStoreFilterData.count){
                cell.bgImg.layer.cornerRadius = 10.0
                cell.lblTitle.text = "\(arrStoreFilterData[indexPath.row]["word"]!)"
                cell.lblType.text = "પ્રકાર : \(arrStoreFilterData[indexPath.row]["type"]!)"
            }

        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        // handle tap events
        if(arrStoreFilterData.count == 0){
            tblViewSearchObj.isHidden = true
            txtsearchBox.resignFirstResponder()
            
            lblTitle.text = "\(arrData[indexPath.row]["word"]!)"
            lblType.text = "પ્રકાર : \(arrData[indexPath.row]["type"]!)"
            lblMeaning.text = "\(arrData[indexPath.row]["meaning"]!)"
        }else{
            tblViewSearchObj.isHidden = true
            txtsearchBox.resignFirstResponder()
            
            lblTitle.text = "\(arrStoreFilterData[indexPath.row]["word"]!)"
            lblType.text = "પ્રકાર : \(arrStoreFilterData[indexPath.row]["type"]!)"
            lblMeaning.text = "\(arrStoreFilterData[indexPath.row]["meaning"]!)"
            
        }
    }
    
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBAction func btnSharePress(sender:UIButton)
    {
        let text = "Word : \(lblTitle.text!)\nMeaning : \(lblMeaning.text!)\n"

        let textToShare = [text,"Download Sarth from\nApp-Store :https://itunes.apple.com/in/app/sarth/id1068164351?mt=8\nPlay Store :https://play.google.com/store/apps/details?id=com.arnion.sarthdict"]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
//        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    @objc func tap(gesture: UITapGestureRecognizer)
    {
        if keyboardSize != CGSize.zero {
            txtsearchBox.resignFirstResponder()
            
        }
    }
    // MARK: - Key Board Show & Hidden Method
    @objc func keyboardWillShow(_ notification: Notification)
    {
        recognizer = UITapGestureRecognizer(target: self, action: #selector(self.tap(gesture:)))
        self.view.addGestureRecognizer(recognizer)
        keyboardSize = ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size)!
    }
    
    @objc func keyboardWillHide(_ notification: Notification)
    {
        keyboardSize = CGSize.zero
        self.view.removeGestureRecognizer(recognizer)
        
    }
    
    // MARK: - Bannerview delegate
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView)
    {
        print("adViewDidReceiveAd")
        //        gadBannerObj = bannerView
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
class MyCollectionViewCell: UICollectionViewCell {
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblType : UILabel!
    @IBOutlet weak var bgImg: UIImageView!
}
import UIKit

public enum DisplayType {
    case unknown
    case iphone4
    case iphone5
    case iphone6
    case iphone6plus
    static let iphone7 = iphone6
    static let iphone7plus = iphone6plus
    case iphoneX
}
public final class Display {
    class var width:CGFloat { return UIScreen.main.bounds.size.width }
    class var height:CGFloat { return UIScreen.main.bounds.size.height }
    class var maxLength:CGFloat { return max(width, height) }
    class var minLength:CGFloat { return min(width, height) }
    class var zoomed:Bool { return UIScreen.main.nativeScale >= UIScreen.main.scale }
    class var retina:Bool { return UIScreen.main.scale >= 2.0 }
    class var phone:Bool { return UIDevice.current.userInterfaceIdiom == .phone }
    class var pad:Bool { return UIDevice.current.userInterfaceIdiom == .pad }
    class var carplay:Bool { return UIDevice.current.userInterfaceIdiom == .carPlay }
    class var tv:Bool { return UIDevice.current.userInterfaceIdiom == .tv }
    class var typeIsLike:DisplayType
    {
        if phone && maxLength < 568 {
            return .iphone4
        }
        else if phone && maxLength == 568 {
            return .iphone5
        }
        else if phone && maxLength == 667 {
            return .iphone6
        }
        else if phone && maxLength == 736 {
            return .iphone6plus
        }
        else if phone && maxLength == 812 {
            return .iphoneX
        }
        return .unknown
    }
}
